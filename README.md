## Contacts
Khách hàng tiềm năng được quản lý bằng contact, ở các bản cũ gọi là lead.
Có 2 loại contact:

* Visitor: Kháng hàng ẩn danh (những người truy cập website mà chưa được định danh)
* Standard contacts: Contact được định danh bằng 1 số cách, thường đã biết tên,email,sđt...
### Quản lý contacts
#### Segments
Có thể chia các contacts thành các nhóm để dễ quản lý
#### Thêm contacts
Có thể thêm bằng tay hoặc import file csv.

#### Các thông tin lưu trong contact
Bao gồm
* Avatar
* Lịch sử tương tác 
* Các ghi chú
* Social: Có thể dùng plugin Facebook/Twitter để theo dõi hoạt động trên mạng xã hội.
* Bản đồ: Có thể track vị trí của contact

#### Contact monitoring
Có nhiều phương pháp theo dõi hoạt động của 1 contact

* Theo dõi qua website dùng JS.
* Mobile monitoring: Hỗ trợ đa nền tảng, cả các app native và app dùng HTML5.
Có các plugin hỗ trợ dành cho Joomla, Drupal, Wordpress..., hỗ trợ tracking qua Google Analytics và Facebook Pixel

## Các kênh tương tác
### Emails
Có 2 loại
* Template mails: Được gửi tự động bởi 1 campaign hay khi đạt đủ điều kiện nào đó (point trigger)
* Segment mails (hay broadcast mails): Có thể dùng để gửi cho tất cả các contact trong 1 segment, mỗi contact chỉ nhận mail này 1 lần.

#### Quản lý email
* Tạo email: Có thể dùng HTML hoặc dùng tool email builder có sẵn của mautic
* Dịch tự động: Cho phép dịch tự động mail theo ngôn ngữ đã set cho contact
* Segment: Có thể gán các segment cần gửi tới cho mỗi emal

#### Theo dõi tương tác email
Tùy từng email service, có thể track các sự kiện: Sent, Opened, Clicked, Unsubcribed...

Mautic có support Elastic Mail, Amazon Mail Service và Mailjet.

### Web notifications
Sử dụng tài khoản OneSignal, có thể gửi noti cho khánh hàng qua trình duyệt khi có permission
### Mobile notifications
Sử dụng tài khoản OneSignal, có thể gửi noti cho thiết bị di động của khách hàng khi có permission, hỗ trợ Andoroid và iOS
### Mautic-Twitter 
Một plugin cho Mautic nhằm track các hoạt động của 1 contact qua Twitter

## Campaigns 
Có 2 loại chiến dịch Marketing
* Time Driven: Xây dựng chiến dịch marketing xung quanh 1 số mốc thời gian cụ thể, các sự kiện thường
được thông báo cho khách hàng qua email.
* Contact Driven: Kích hoạt các sự kiện dựa trên các tương tác với contact ( khi contact mở email, truy cập website...).
Các sự kiện này có thể được mail đến ngay lập tức hoặc mail sau.
* Mixed Campaigns: Kết hợp cả 2 loại trên.

Ngoài email còn có thể có các loại tương tác khác, ví dụ chuyển contact sang segment khác, cộng điểm ...
### Quản lý chiến dịch marketing
#### Tạo chiến dịch
Về cơ bản bao gồm chọn tên, chọn mô tả, chọn các segment mà chiến dịch target đến.

#### Campaign builder
Cho phép vẽ ra workflow cho 1 campaign, mà sau đó có thể được hệ thống thực hiện tự động.

* Chọn source: Có 2 kiểu source là contact segement và contact form - những người đã submit 1 form đã được định nghĩa trước sẽ 
được thêm tự động vào chiến dịch.

![source](https://mautic.org/docs/en/campaigns/media/contact-sources.png)

* Bước tiếp theo là chọn 1 action và decision/condition

    * Action là các hành động do chính mình khởi tạo, khi bắt đầu 1 campaign action
    action sẽ được thực hiện đầu tiên.
    
    ![action](https://mautic.org/docs/en/campaigns/media/send-email-delay.png)
    
        Các action gồm có:
        
        * Add to company's score
        * Add to company action
        * Adjust contact points
        * Change campaigns
        * Change contact's stage
        * Delete contact
        * Modify contact's segments
        * Modify contact's tags
        * Push contact to integration
        * Send a webhook
        * Send email
        * Send email to user
        * Send marketing message
        * Update contact

    * Decision được khởi tạo bởi contact, vd như mở mail, submit form... Trong campaign builder các decision được 
    thể hiện bởi 1 decision tree có 2 nhánh, 1 nhánh sẽ được kích hoạt khi contact thực hiện hành động và 1 nhánh kích hoạt 
    khi contact không thực hiện hành động
    ![decision](https://i.imgur.com/13B1Cpq.png)
            
            Các decision gồm có:
        
            * Clicks email
            * Device visit
            * Downloads asset
            * Opens email
            * Request dynamic content
            * Submits form
            * Visits a page
        
    * Condition có thể được dùng để kích hoạt các hành động dựa vào các data thu được từ việc tương tác với contact.
       
        Các data bao gồm:
    
        * Contact device
        * Contact field value
        * Contact owner
        * Contact segments
        * Contact tags
        * Form field value
        
![campaign](https://i.imgur.com/Ih4YtUf.png)